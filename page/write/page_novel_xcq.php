<?php
session_start();
/* 
 * See if the $_SESSION['novel'] is properly set before moving.
 */
if(!filter_has_var(INPUT_POST, 'page')) {die('Error : No page selected.');}
if(!filter_has_var(INPUT_POST, 'context')) {die('Error : No context sent.');}
if(!filter_has_var(INPUT_POST, 'change')) {die('Error : No submit button pressed.');}
$novel = $_SESSION['novel']['id'];
$page = filter_input(INPUT_POST, 'page');
$context = filter_input(INPUT_POST, 'context');
require_once __DIR__.'/../../bin/sql/connect.php';
switch (filter_input(INPUT_POST, 'change')) {
    case 'update': {
        $query = "UPDATE `pages` SET `novel`=?, `context`=? WHERE `page` = ?";
        $types = 'isi';
        if(!mysqli_stmt_prepare($stmt, $query))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        if(!mysqli_stmt_bind_param($stmt,$types,$novel,$context,$page))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        if(!mysqli_stmt_execute($stmt))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        break;
    }
    case 'delete': {
        $query_1 = "DELETE FROM `pages` WHERE `page` = ?";
        $types = 'i';
        if(!mysqli_stmt_prepare($stmt, $query_1))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        if(!mysqli_stmt_bind_param($stmt,$types,$page))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        if(!mysqli_stmt_execute($stmt))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        $query_2 = "DELETE FROM `indice` WHERE `back` = ?";
        if(!mysqli_stmt_prepare($stmt, $query_2))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        if(!mysqli_stmt_bind_param($stmt,$types,$page))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        if(!mysqli_stmt_execute($stmt))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        $query_3 = "DELETE FROM `indice` WHERE `next` = ?";
        if(!mysqli_stmt_prepare($stmt, $query_3))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        if(!mysqli_stmt_bind_param($stmt,$types,$page))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        if(!mysqli_stmt_execute($stmt))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        break;
    }
    default: {break;}
}
require_once __DIR__.'/../../bin/sql/disconnect.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Change Finished</title>
    </head>
    <body>
        <div>
            <h4>Your attempt to <?php echo filter_input(INPUT_POST, 'change'); ?> is a success.</h4>
            <a href="table_novel_context.php"><p>Return to page table.</p></a>
        </div>
    </body>
</html>