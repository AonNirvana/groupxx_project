<?php
session_start();
/* 
 * See if the $_SESSION['novel'] is properly set before moving.
 */
require_once __DIR__.'/../../bin/sql/connect.php';
$query_1 = "INSERT INTO `".groupXX_database."`.`pages` (`novel`, `page`, `context`) VALUES (?, ?, ?)";
$novel = $_SESSION['novel']['id'];
$page = null;
$context = '';
$types_1 = 'iis'; // There are another types after this.

if(!mysqli_stmt_prepare($stmt, $query_1))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_param($stmt,$types_1,$novel,$page,$context))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_execute($stmt))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
$new_page = mysqli_stmt_insert_id($stmt);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Page <?php echo $new_page; ?> created</title>
    </head>
    <body>
        <div class="container">
            <h4>You have created a new page.</h4>
            <p>Its page number is <?php echo $new_page; ?></p>
            <p>You can edit the context later.</p>
            <p>Now is your sole chance to link the create page.</p>
            <a href="page_novel_context.php?page=<?php echo $new_page; ?>"><p>Go to edit context.</p></a>
        </div>
        <div class="container">
            <h5>Page to insert.</h5>
            <?php
            $query_2 = "SELECT `page` FROM `pages` WHERE `novel` = ?";
            $types_2 = 'i';
            if(!mysqli_stmt_prepare($stmt, $query_2))
            {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
            if(!mysqli_stmt_bind_param($stmt, $types_2, $novel))
            {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
            if(!mysqli_stmt_execute($stmt))
            {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
            if(!mysqli_stmt_bind_result($stmt, $old_page))
            {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
            ?>
            <form class="col-sm-6" role="form" method="post" action="page_novel_link.php" target="_blank">
                <input type="hidden" id="new_page" name="new_page" value="<?php echo $new_page; ?>"/>
                <div class="form-group">
                    <label for="old_page">Page</label>
                    <select class="form-control" id="old_page" name="old_page">
                    <?php while(mysqli_stmt_fetch($stmt)) { ?>
                        <option value="<?php echo $old_page; ?>"><?php echo $old_page; ?></option>
                    <?php } ?>
                    </select>
                </div>
                <button class="btn btn-info" type="submit" id="col" name="col" value="back">Insert as previous page.</button>
                <button class="btn btn-primary" type="submit" id="col" name="col" value="next">Insert as next page.</button>
            </form>
        </div>
    </body>
</html>
<?php
require_once __DIR__.'/../../bin/sql/disconnect.php';
?>