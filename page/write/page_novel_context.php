<?php
session_start();
/* 
 * See if the $_SESSION['novel'] is properly set before moving.
 */
if(!filter_has_var(INPUT_GET, 'page')) {die('Error : No page selected.');}
$page = filter_input(INPUT_GET, 'page');
require_once __DIR__.'/../../bin/sql/connect.php';
$query_1 = "SELECT `novel`, `context` FROM `pages` WHERE `page` = ?";
$types = 'i'; // Used this type across other queries in this page.
if(!mysqli_stmt_prepare($stmt, $query_1))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_param($stmt, $types, $page))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_execute($stmt))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_result($stmt, $novel, $context))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
while(mysqli_stmt_fetch($stmt)) {break;} // I expected to have only one page per page.
if($novel != $_SESSION['novel']['id']) {
    require_once __DIR__.'/../../bin/sql/disconnect.php';
    exit(1);
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Now editing</title>
    </head>
    <body>
        <div class="container">
            <h4>Page <?php echo $page ?> of novel <?php echo $_SESSION['novel']['id']; ?> - <?php echo $_SESSION['novel']['name']; ?></h4>
            <p>Warning : You are NOT allowed to edit page link list AFTER created the page.</p>
            <p>Warning 2: Delete page will delete links from and to it.</p>
            <form class="col-sm-6" role="form" method="post" action="page_novel_xcq.php">
                <input type="hidden" id="page" name="page" value="<?php echo $page; ?>"/>
                <div class="form-group">
                    <textarea class="form-control" id="context" name="context" maxlength="512" rows="15"><?php echo $context; ?></textarea>
                </div>
                <button class="btn btn-info" type="reset">Reset</button>
                <button class="btn btn-success" type="submit" id="change" name="change" value="update">Update</button>
                <button class="btn btn-danger" type="submit" id="change" name="change" value="delete">Delete</button>
            </form>
        </div>
        <div class="container">
            <h5>Page prior to this page.</h5>
            <?php
            $query_2 = "SELECT `back` FROM `indice` WHERE `next` = ?";
            if(!mysqli_stmt_prepare($stmt, $query_2))
            {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
            if(!mysqli_stmt_bind_param($stmt, $types, $page))
            {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
            if(!mysqli_stmt_execute($stmt))
            {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
            if(!mysqli_stmt_bind_result($stmt, $back))
            {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
            ?>
            <table>
            <?php while(mysqli_stmt_fetch($stmt)) { ?>
                <tr>
                    <td>
                        <a href="page_novel_context.php?page=<?php echo $back; ?>"><p>page <?php echo $back; ?></p></a>
                    </td>
                </tr>
            <?php } ?>
            </table>
        </div>
        <div class="container">
            <h5>Page next to this page</h5>
            <?php
            $query_3 = "SELECT `next` FROM `indice` WHERE `back` = ?";
            if(!mysqli_stmt_prepare($stmt, $query_3))
            {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
            if(!mysqli_stmt_bind_param($stmt, $types, $page))
            {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
            if(!mysqli_stmt_execute($stmt))
            {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
            if(!mysqli_stmt_bind_result($stmt, $next))
            {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
            ?>
            <table>
            <?php while(mysqli_stmt_fetch($stmt)) { ?>
                <tr>
                    <td>
                        <a href="page_novel_context.php?page=<?php echo $next; ?>"><p>page <?php echo $next; ?></p></a>
                    </td>
                </tr>
            <?php } ?>
            </table>
        </div>
    </body>
</html>
<?php
require_once __DIR__.'/../../bin/sql/disconnect.php';
?>