<?php
if(!filter_has_var(INPUT_POST,'name')) {die('Error : This novel has no name.');}
if(!filter_has_var(INPUT_POST, 'id')) {die('Error : Please spec an ID.');}
if(!filter_has_var(INPUT_POST,'password')) {die('Error : Please enter a password.');}
$name = filter_input(INPUT_POST, 'name');
$id = filter_input(INPUT_POST, 'id');
$old_password = filter_input(INPUT_POST, 'password');

require_once __DIR__.'/../../bin/sql/connect.php';
$query = "SELECT `password` FROM `novel` WHERE `id` = ?";
$types = 'i';
if(!mysqli_stmt_prepare($stmt, $query))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_param($stmt,$types,$id))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_execute($stmt))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_result($stmt, $password))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
while(mysqli_stmt_fetch($stmt)) {
    session_start();
    if(password_verify($old_password, $password)) {
        $_SESSION['novel']['name'] = $name;
        $_SESSION['novel']['id'] = $id;
        $_SESSION['novel']['password'] = $password;
    } else {
        if(isset($_SESSION['novel'])) {unset($_SESSION['novel']);}
    }
    break;
}
require_once __DIR__.'/../../bin/sql/disconnect.php';
if(isset($_SESSION['novel'])) {
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title><?php echo $_SESSION['novel']['name']; ?> authorized</title>
    </head>
    <body>
        <div>
            <h4>You gain authority to editing a novel.</h4>
            <p>Its ID is <?php echo $_SESSION['novel']['id']; ?></p>
            <a href="table_novel_context.php"><p>Continue to edit the authorized novel.</p></a>
        </div>
    </body>
</html>
<?php } else {?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Authorization Failed</title>
    </head>
    <body>
        <div>
            <h4>You gain no authority to editing a novel.</h4>
            <a href="../../index.php"><p>Return to the front page.</p></a>
        </div>
    </body>
</html>
<?php } ?>
