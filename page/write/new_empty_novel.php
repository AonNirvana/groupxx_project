<?php
if(!filter_has_var(INPUT_POST,'name')) {die('Error : This novel has no name.');}
if(!filter_has_var(INPUT_POST,'password')) {die('Error : Please enter a password.');}
$name = filter_input(INPUT_POST,'name');
$id = null;
$new_password = filter_input(INPUT_POST,'password');
$password = password_hash($new_password, CRYPT_BLOWFISH);

require_once __DIR__.'/../../bin/sql/connect.php';
$query = "INSERT INTO `".groupXX_database."`.`novel` (`name`, `id`, `password`) VALUES (?, ?, ?)";
$types = 'sis';
if(!mysqli_stmt_prepare($stmt, $query))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_param($stmt,$types,$name,$id,$password))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_execute($stmt))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
$new_id = mysqli_stmt_insert_id($stmt);
require_once __DIR__.'/../../bin/sql/disconnect.php';

session_start();
$_SESSION['novel']['name'] = $name;
$_SESSION['novel']['id'] = $new_id;
$_SESSION['novel']['password'] = $password;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Novel <?php echo $_SESSION['novel']['name']; ?> created</title>
    </head>
    <body>
        <div>
            <h4>You create a new novel.</h4>
            <p>Its ID is <?php echo $_SESSION['novel']['id']; ?></p>
            <p>Enter password <?php $new_password;  ?> to gain authority.</p>
            <a href="table_novel_context.php"><p>For now, you can just move to the new novel.</p></a>
        </div>
    </body>
</html>