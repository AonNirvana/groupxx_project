<?php
session_start();
/* 
 * See if the $_SESSION['novel'] is properly set before moving.
 */
require_once __DIR__.'/../../bin/sql/connect.php';
$query = "SELECT `page` FROM `pages` WHERE `novel` = ?";
$types = 'i';
if(!mysqli_stmt_prepare($stmt, $query))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_param($stmt, $types, $_SESSION['novel']['id']))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_execute($stmt))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_result($stmt, $page))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Author Mode</title>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        #<?php echo $_SESSION['novel']['id']; ?> - <?php echo $_SESSION['novel']['name']; ?>
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                            <?php while(mysqli_stmt_fetch($stmt)) { ?>
                                <li><a href="page_novel_context.php?page=<?php echo $page; ?>" target="pointed-frame"><?php echo $page; ?></a></li>
                            <?php } ?>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="page_novel_craft.php" target="pointed-frame">Create</a></li>
                        <li><a href="../../index.php">Exit</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="embed-responsive embed-responsive-4by3">
            <iframe class="embed-responsive-item" id="pointed-frame" name="pointed-frame"></iframe>
        </div>
    </body>
</html>
<?php
require_once __DIR__.'/../../bin/sql/disconnect.php';
?>