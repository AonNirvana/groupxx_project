<?php
session_start();
/* 
 * See if the $_SESSION['novel'] is properly set before moving.
 */
if(!filter_has_var(INPUT_POST, 'new_page')) {die('No (new) page selected.');}
if(!filter_has_var(INPUT_POST, 'old_page')) {die('No (old) page selected.');}
if(!filter_has_var(INPUT_POST, 'col')) {die('No submit button pressed.');}
$novel = $_SESSION['novel']['id'];
$new_page = filter_input(INPUT_POST, 'new_page');
$old_page = filter_input(INPUT_POST, 'old_page');

require_once __DIR__.'/../../bin/sql/connect.php';
$query = "INSERT INTO `".groupXX_database."`.`indice` (`novel`, `back`, `next`) VALUES (?, ?, ?)";
$types = 'iii';
if(!mysqli_stmt_prepare($stmt, $query))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
switch (filter_input(INPUT_POST, 'col')) {
    case 'back': {
        if(!mysqli_stmt_bind_param($stmt,$types,$novel,$old_page,$new_page))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        break;
    }
    case 'next': {
        if(!mysqli_stmt_bind_param($stmt,$types,$novel,$new_page,$old_page))
        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
        break;
    }
    default: {break;}
}
if(!mysqli_stmt_execute($stmt))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
require_once __DIR__.'/../../bin/sql/disconnect.php';
echo 'Page link success. You can close this page.';