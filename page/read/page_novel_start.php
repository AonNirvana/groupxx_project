<?php
if(!filter_has_var(INPUT_GET, 'name')) {die('Error : This novel has no name.');}
if(!filter_has_var(INPUT_GET, 'id')) {die('Error : This novel has no ID.');}
$name = filter_input(INPUT_GET, 'name');
$id = filter_input(INPUT_GET, 'id');

$query = "SELECT MIN(`page`) FROM `pages` WHERE `novel` = ?";
$types = 'i';
require_once __DIR__.'/../../bin/sql/connect.php';
if(!mysqli_stmt_prepare($stmt, $query))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_param($stmt, $types, $id))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_execute($stmt))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_result($stmt, $minpage))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
while(mysqli_stmt_fetch($stmt)) {break;}
require_once __DIR__.'/../../bin/sql/disconnect.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Reader Mode</title>
    </head>
    <body>
        <div>
            <h4>You are reading the novel <?php echo $id; ?> - <?php echo $name; ?> .</h4>
            <h4>This novel start from page <?php echo $minpage; ?></h4>
            <a href="page_novel_frame.php?page=<?php echo $minpage; ?>" target="_blank"><p>Start reading.</p></a>
            <a href="../../index.php"><p>Return to the front page.</p></a>
        </div>
    </body>
</html>