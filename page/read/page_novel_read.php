<?php
if(!filter_has_var(INPUT_GET, 'page')) {die('Error : No page selected.');}
$page = filter_input(INPUT_GET, 'page');

require_once __DIR__.'/../../bin/sql/connect.php';

$tag['a']['back'] = [];
$tag['a']['next'] = [];
$tag['p']['context'] = [];

$query_1 = "SELECT `context` FROM `pages` WHERE `page` = ?";
$types = 'i'; // Used this type across other queries in this page.
if(!mysqli_stmt_prepare($stmt, $query_1))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_param($stmt, $types, $page))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_execute($stmt))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_result($stmt, $context))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
while(mysqli_stmt_fetch($stmt)) {
    $tag['p']['context'][] = $context;
    break; // I expected to have only one page per page.
}

$query_2 = "SELECT `back` FROM `indice` WHERE `next` = ?";
if(!mysqli_stmt_prepare($stmt, $query_2))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_param($stmt, $types, $page))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_execute($stmt))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_result($stmt, $back))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
while(mysqli_stmt_fetch($stmt)) {
    $tag['a']['back'][] = $back;
}

$query_3 = "SELECT `next` FROM `indice` WHERE `back` = ?";
if(!mysqli_stmt_prepare($stmt, $query_3))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_param($stmt, $types, $page))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_execute($stmt))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_result($stmt, $next))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
while(mysqli_stmt_fetch($stmt)) {
    $tag['a']['next'][] = $next;
}
require_once __DIR__.'/../../bin/sql/disconnect.php';
?>
<?php
header('Content-type: text/xml');
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
echo "<root>\n";
if(!empty($tag['p']['context'])) {
    echo "\t<p>".$tag['p']['context'][0]."</p>\n";
} else {echo '<p></p>';}
echo "\t<div>\n";
if(!empty($tag['a']['next'])) {
    for($i=0;$i<count($tag['a']['next']);$i++) {
        echo "\t\t<a href=\"page_novel_frame.php?page=".$tag['a']['next'][$i]."\"><p>Path ".$i."</p></a>\n";
    }
} else {
    echo "\t\t<p>Thx for reading.</p>\n";
    echo "\t\t<p>This story branch is over.</p>\n";
    echo "\t\t<p>You can navigate back and try another.</p>\n";
}
echo "\t</div>";
echo "</root>";
?>