<?php
if(!filter_has_var(INPUT_GET, 'page')) {die('Error : No page selected.');}
$page = filter_input(INPUT_GET, 'page');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Now reading</title>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.debug.js"></script>
    </head>
    <body>
        <div>
            <div class="container" id="frame" name="frame">
                <p>Now Loading</p>
            </div>
            <div class="container">
                <button class="btn btn-info" type="button" id="dopdfmagic" name="dopdfmagic" onclick="printPDF()">Do PDF magic.</button>
            </div>
            <script type="text/javascript">
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (xhttp.readyState === 4 && xhttp.status === 200) {
                        document.getElementById("frame").innerHTML = xhttp.responseText;
                    }
                };
                xhttp.open("GET","page_novel_read.php?page=<?php echo $page; ?>",true);
                xhttp.send();
            </script>
            <script type="text/javascript">
                function printPDF() {
                    var doc = new jsPDF();
                    var elementHandler = {
                        '#ignorepdf': function(element,renderer) {
                            return true;
                        }
                    }
                    var source = window.document.getElementById('frame');
                    doc.fromHTML(
                        source,
                        15,
                        15,
                        {
                            'width':180,
                            'elementHandlers': elementHandler
                        }
                    );
                    doc.save('dopdfmagic.pdf');
                }
            </script>
            <div></div>
        </div>
    </body>
</html>