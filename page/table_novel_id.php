<?php
session_start();
if(isset($_SESSION['novel'])) {unset($_SESSION['novel']);}

if(!filter_has_var(INPUT_GET, 'queve')) {die('Error : Please enter a name keyword');}
$queve = '%'.filter_input(INPUT_GET, 'queve').'%';

require_once __DIR__.'/../bin/sql/connect.php';
$query = "SELECT `name`, `id` FROM `novel` WHERE `name` LIKE ?";
$types = "s";
if(!mysqli_stmt_prepare($stmt, $query))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_param($stmt, $types, $queve))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_execute($stmt))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_stmt_bind_result($stmt, $name,$id))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Search Result</title>
    </head>
    <body>
        <div class="container">
            <h4>Keyword : <?php echo $queve; ?></h4>
            <table class="table">
                <tr class="primary">
                    <td>Name</td>
                    <td>ID</td>
                    <td>Authorize</td>
                    <td>Read</td>
                </tr>
            <?php while(mysqli_stmt_fetch($stmt)) { ?>
                <tr class="info">
                    <td><?php echo $name; ?></td>
                    <td><?php echo $id; ?></td>
                    <td>
                        <form class="form-inline" role="form" method="post" action="write/novel_write_authorize.php">
                            <input type="hidden" id="name" name="name" value="<?php echo $name; ?>"/>
                            <input type="hidden" id="id" name="id" value="<?php echo $id; ?>"/>
                            <div class="form-group">
                                <input class="form-control" id="password" name="password" type="password" maxlength="72" autocomplete="off"/>
                            </div>
                            <button class="btn btn-default" type="submit">Authorize</button>
                        </form>
                    </td>
                    <td>
                        <form class="form-inline" role="form" method="get" action="read/page_novel_start.php">
                            <input type="hidden" id="name" name="name" value="<?php echo $name; ?>"/>
                            <input type="hidden" id="id" name="id" value="<?php echo $id; ?>"/>
                            <button class="btn btn-default" type="submit">Read</button>
                        </form>
                    </td>
                </tr>
            <?php } ?>
            </table>
        </div>
    </body>
</html>
<?php
require_once __DIR__.'/../bin/sql/disconnect.php';
?>