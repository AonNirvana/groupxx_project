-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2016 at 02:12 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `groupxx`
--
CREATE DATABASE IF NOT EXISTS `groupxx` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
USE `groupxx`;

-- --------------------------------------------------------

--
-- Table structure for table `indice`
--

DROP TABLE IF EXISTS `indice`;
CREATE TABLE IF NOT EXISTS `indice` (
  `novel` bigint(20) NOT NULL COMMENT 'The ID of the novel.',
  `back` bigint(20) NOT NULL COMMENT 'WHERE here for the list of next pages.',
  `next` bigint(20) NOT NULL COMMENT 'WHERE here for the list of previous pages.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- RELATIONS FOR TABLE `indice`:
--   `novel`
--       `novel` -> `id`
--   `back`
--       `pages` -> `page`
--   `next`
--       `pages` -> `page`
--

-- --------------------------------------------------------

--
-- Table structure for table `novel`
--

DROP TABLE IF EXISTS `novel`;
CREATE TABLE IF NOT EXISTS `novel` (
  `name` char(255) COLLATE utf8mb4_bin NOT NULL COMMENT 'Can have same name over several novels.',
  `id` bigint(20) NOT NULL,
  `password` char(60) COLLATE utf8mb4_bin NOT NULL COMMENT 'Crypt Blowfish'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- RELATIONS FOR TABLE `novel`:
--

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `novel` bigint(20) NOT NULL COMMENT 'The ID of the novel.',
  `page` bigint(20) NOT NULL COMMENT 'WHERE here for the current page.',
  `context` text COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- RELATIONS FOR TABLE `pages`:
--   `novel`
--       `novel` -> `id`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `indice`
--
ALTER TABLE `indice`
  ADD PRIMARY KEY (`back`,`next`),
  ADD KEY `page_back` (`back`),
  ADD KEY `page_next` (`next`),
  ADD KEY `novel_current` (`novel`);

--
-- Indexes for table `novel`
--
ALTER TABLE `novel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page`),
  ADD KEY `novel_current` (`novel`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `novel`
--
ALTER TABLE `novel`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'WHERE here for the current page.';
--
-- Constraints for dumped tables
--

--
-- Constraints for table `indice`
--
ALTER TABLE `indice`
  ADD CONSTRAINT `indice_ibfk_1` FOREIGN KEY (`novel`) REFERENCES `novel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indice_ibfk_2` FOREIGN KEY (`back`) REFERENCES `pages` (`page`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `indice_ibfk_3` FOREIGN KEY (`next`) REFERENCES `pages` (`page`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pages`
--
ALTER TABLE `pages`
  ADD CONSTRAINT `pages_ibfk_1` FOREIGN KEY (`novel`) REFERENCES `novel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
