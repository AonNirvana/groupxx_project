<?php
/*
 * Head of MySQLi.
 * Should be called before any MySQLi operations.
 */
require_once __DIR__.'/server.php';
$link = mysqli_init();
if(!mysqli_real_connect($link, groupXX_host, groupXX_user, groupXX_password, groupXX_database, groupXX_port))
{die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
$stmt = mysqli_stmt_init($link);