<?php
/*
 * Foot of MySQLi things.
 * Should be required at the end of using MySQLi.
 * $link and $stmt should be in connect.php, required somewhere before require this.
 */
if(!mysqli_stmt_close($stmt)) {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
if(!mysqli_close($link)) {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}