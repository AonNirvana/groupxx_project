<?php
session_start();
if(isset($_SESSION['novel'])) {unset($_SESSION['novel']);}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <title>Welcome to a simple novel reader.</title>
    </head>
    <body>
        <div class="container">
            <h4>Welcome</h4>
            <p>You can search for a novel you wish to read, and if you have the</p>
            <p>authority for a novel, you might be able to edit it as well.</p>
            <div class="row">
                <h5>Find a novel</h5>
                <form class="col col-sm-6" role="form" method="get" action="page/table_novel_id.php">
                    <div class="form-group">
                        <?php
                        require_once __DIR__.'/bin/sql/connect.php';
                        $query = 'SELECT DISTINCT `name` FROM `novel`';
                        if(!mysqli_stmt_prepare($stmt, $query))
                        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
                        if(!mysqli_stmt_execute($stmt))
                        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
                        if(!mysqli_stmt_bind_result($stmt, $name))
                        {die('MySQLi Error '.mysqli_errno($link).': '.mysqli_error($link).'<br/>');}
                        ?>
                        <datalist id="novel_name" name="novel_name">
                        <?php while(mysqli_stmt_fetch($stmt)) { ?>
                            <option value="<?php echo $name; ?>"><?php echo $name; ?></option>
                        <?php } ?>
                        </datalist>
                        <?php
                        require_once __DIR__.'/bin/sql/disconnect.php';
                        ?>
                        <label for="queve">(Part of) Name</label>
                        <input class="form-control" id="queve" name="queve" type="text" maxlength="255" list="novel_name" autocomplete="off"/>
                    </div>
                    <button class="btn btn-default" type="submit">Find</button>
                </form>
            </div>
            <div class="row">
                <h5>New novel</h5>
                <form class="col col-sm-6" role="form" method="post" action="page/write/new_empty_novel.php">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input class="form-control" id="name" name="name" type="text" maxlength="255" autocomplete="off"/>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control" id="password" name="password" type="password" maxlength="72" autocomplete="off"/>
                    </div>
                    <button class="btn btn-default" type="submit">Move</button>
                </form>
            </div>
        </div>
    </body>
</html>
